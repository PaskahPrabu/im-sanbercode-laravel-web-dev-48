1. Buat Database
CREATE DATABASE myshop

2. Buat Table
>> users
CREATE TABLE users( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) 
);

>> categories
CREATE TABLE categories( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255)
);

>> items
CREATE TABLE items( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN KEY (category_id) REFERENCES categories(id) 
);

3. Memasukkan Data
>> users
INSERT INTO 
	users(name, email, password)
VALUES 
	('John Doe', 'john@doe.com', 'john123'),
	('Jane Doe', 'jane@doe.com', 'jenita123');

>> categories
INSERT INTO 
	categories(name) 
VALUES 
	('gadget'),('cloth'),('men'),('women'),('branded');

>> items
INSERT INTO 
	items(name, description, price, stock, category_id) 
VALUES 
	('Sumsang b50', 'hape keren dari merk sumsang', 4000000, 100, 1),
	('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
	('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

4. Mengambil Data dari Database
- 4a
SELECT id, name, email FROM users;

- 4b
SELECT * FROM items WHERE price > 1000000
SELECT * FROM items WHERE name LIKE '%watch';

- 4c
SELECT 
	items.name AS name,
	items.description AS description,
	items.price AS price,
	items.stock AS stock, 
	items.category_id AS category_id,
	categories.name AS kategori
FROM items
JOIN categories
ON items.category_id = categories.id

5. Mengubah Data
UPDATE items SET price=2500000 WHERE name = 'Sumsang b50';