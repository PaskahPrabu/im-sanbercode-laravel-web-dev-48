<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profile';

    // Kolom yang dapat dimanipulasi (CRUD)
    protected $fillable = ['age', 'bio', 'address', 'user_id'];

}
