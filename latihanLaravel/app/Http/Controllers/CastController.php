<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // --- Create Data ---
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // validation data
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric|max:99',
            'bio' => 'required',
        ]);

        // insert data
        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio')
        ]);

        // arahkan ke halaman cast
        return redirect('/cast');
    }

    // --- Show Data ---
    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $castData = DB::table('cast')->find($id);

        return view('cast.detail', ['castData' => $castData]);
    }

    // --- Update Data ---
    public function edit($id)
    {
        $castData = DB::table('cast')->find($id);

        return view('cast.edit', ['castData' => $castData]);
    }

    public function update($id, Request $request)
    {
        // validation data
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric|max:99',
            'bio' => 'required',
        ]);

        // update data
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request->input('name'),
                    'age' => $request->input('age'),
                    'bio' => $request->input('bio')
                ]
                );

        // arahkan ke halaman cast
        return redirect('/cast');
    }

    // --- Delete Data ---
    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}