<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genres;
use App\Models\Film;
use File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', ['film'=>$film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genres::all();
        return view('film.create', ['genres' => $genres]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'poster' => 'required|mimes:png,jpg,jpeg|max:2048',
            'title' => 'required',
            'year' => 'required',
            'genre_id' => 'required',
            'summary' => 'required',
        ]);

        $posterName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('image'), $posterName);
    
        $film = New Film;

        $film->title = $request->input('title');
        $film->year = $request->input('year');
        $film->genre_id = $request->input('genre_id');
        $film->summary = $request->input('summary');
        $film->poster = $posterName;

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        return view('film.detail', ['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genres = Genres::all();
        return view('film.edit', ['genres' => $genres, 'film'=>$film]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'poster' => 'mimes:png,jpg,jpeg|max:2048',
            'title' => 'required',
            'year' => 'required',
            'genre_id' => 'required',
            'summary' => 'required',
        ]);

        $film = Film::find($id);

        if($request->has('poster')) {
            $path = "image/";
            File::delete($path . $film->poster);

            $posterName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('image'), $posterName);
            
            $film->poster = $posterName;
        }

        $film->title = $request->input('title');
        $film->year = $request->input('year');
        $film->genre_id = $request->input('genre_id');
        $film->summary = $request->input('summary');

        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $path = "image/";
        File::delete($path . $film->poster);

        $film->delete();

        return redirect('/film');
    }
}
