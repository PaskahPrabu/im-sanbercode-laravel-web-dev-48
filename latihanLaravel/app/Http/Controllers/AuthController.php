<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signUp()
    {
        return view('page.form');
    }

    public function welcome(Request $request)
    {
        $firstName = $request->input('first-name');
        $lastName = $request->input('last-name');

        return view(
            'page.welcome', 
            ['firstName' => $firstName, 'lastName' => $lastName]
        );
    }
}
