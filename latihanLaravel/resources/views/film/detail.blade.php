@extends('layouts.master')

@section('title')
  Detail Film
@endsection

@section('content')
<img src="{{asset('/image/'.$film->poster)}}" width="100%" height="300px" alt="">

<h1 class="text-info my-2">{{$film->title}}</h1>
<p>{{$film->summary}}</p>

<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>
@endsection