@extends('layouts.master')

@section('title')
  Edit Film
@endsection

@section('content')
<form method="post" action="/film/{{$film->id}}" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Judul Film</label>
    <input type="text" name="title" value="{{$film->title}}" class="form-control @error('title') is-invalid @enderror">
  </div>
  @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Tahun</label>
    <input type="text" name="year" value="{{$film->year}}" class="form-control @error('year') is-invalid @enderror">
  </div>
  @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control" id="">
      <option value="">-- Pilih Genre --</option>
      @forelse ($genres as $item)
        @if ($item->id === $film->genre_id)
          <option value="{{$item->id}}" selected>{{$item->name}}</option>  
        @else
        <option value="{{$item->id}}">{{$item->name}}</option>
        @endif
        
      @empty
        <option value="">Tidak Ada Genre</option>
      @endforelse
    </select>
  </div>
  @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="summary" id="" cols="30", rows="10" class="form-control @error('summary') is-invalid @enderror">{{$film->summary}}</textarea>
  </div>
  @error('summary')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Poster Film</label>
    <input type="file" name="poster" class="form-control @error('poster') is-invalid @enderror">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
