@extends('layouts.master')

@section('title')
  Daftar Film
@endsection

@section('content')

@auth
  <a href="/film/create" class="btn btn-primary btn-sm my-3">Tambah Film</a>
@endauth

<div class="row">
  @forelse ($film as $item)
    <div class="col">
      <div class="card">
        <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" alt="Card image cap">
        <div class="card-body">
          <h2>{{$item->title}}</h2>
          <p class="card-text">{{Str::limit($item->summary, 50)}}</p>
          <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Read More</a>

          @auth
            <div class="row my-2">
              <div class="col">
                <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
              </div>
              <div class="col">
                <form action="/film/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                </form>
              </div>
            </div>
          @endauth
        </div>
      </div>
    </div>
    
  @empty
    <div>
      <h3>Tidak Ada Film</h3>
    </div>
  @endforelse
</div>

@endsection