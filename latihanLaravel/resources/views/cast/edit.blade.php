@extends('layouts.master')

@section('title')
  Edit Data Pemeran
@endsection

@section('content')
<form method="post" action="/cast/{{$castData->id}}">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Name Lengkap</label>
    <input type="text" name="name" value="{{$castData->name}}" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="age" value="{{$castData->age}}" class="form-control @error('age') is-invalid @enderror" placeholder="Masukkan Umur">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Biodata</label>
    <textarea name="bio" id="" cols="30", rows="10" class="form-control @error('bio') is-invalid @enderror" placeholder="Masukkan Biodata">{{$castData->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
