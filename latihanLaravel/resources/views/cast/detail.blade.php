@extends('layouts.master')

@section('title')
  Detail Pemeran
@endsection

@section('content')
<h1 class="text-primary">{{$castData->name}}</h1>
<p class="font-weight-bold">Umur : {{$castData->age}}</p>
<p class="font-weight-bold">Biodata :</p>
<p>{{$castData->bio}}</p>
@endsection