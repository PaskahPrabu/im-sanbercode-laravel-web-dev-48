@extends('layouts.master')

@section('title')
Buat Account Baru !
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="first-name"><br><br>
        <!--  -->
        <label>Last Name:</label><br>
        <input type="text" name="last-name"><br><br>
        <!--  -->
        <label>Gender:</label><br>
        <input type="radio" value="1" name="gender">Male<br>
        <input type="radio" value="2" name="gender">Female<br>
        <input type="radio" value="3" name="gender">Other<br><br>
        <!--  -->
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">United Kingdom</option>
            <option value="3">Japan</option>
        </select><br><br>
        <!--  -->
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="indonesia">Indonesia<br>
        <input type="checkbox" name="english">English<br>
        <input type="checkbox" name="other">Other<br><br>
        <!--  -->
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="40"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
