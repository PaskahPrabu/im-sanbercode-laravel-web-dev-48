<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// get
Route::get('/', [DashboardController::class, 'home']);
Route::get('/register', [AuthController::class, 'signUp']);
Route::get('/table', function(){
  return view('page.table');
});
Route::get('/data-table', function(){
  return view('page.data-table');
});

// post
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::group(['middleware' => ['auth']], function () {
  // Film Review
  // CRUD Cast
  // C -> Create Data
  // create cast
  Route::get('/cast/create', [CastController::class, 'create']);
  // store cast data to database & validation
  Route::post('/cast', [CastController::class, 'store']);

  // R -> Read Data
  // view data
  Route::get('/cast', [CastController::class, 'index']);
  // detail data by id
  Route::get('/cast/{id}', [CastController::class, 'show']);

  // U -> Update Data
  // form update data by id
  Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
  // store updated data by id
  Route::put('/cast/{id}', [CastController::class, 'update']);

  // D -> Delete Data
  // delete data by id
  Route::delete('/cast/{id}', [CastController::class, 'destroy']);
});

// ORM
Route::resource('film', FilmController::class);

// Auth
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
